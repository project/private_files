$(document).ready(function(){
  $("#edit-node-wrapper").hide();
  $("#edit-keywords-wrapper").hide();
  $("#edit-action").change(function(){
    if($("#edit-action").val() == 'attach'){
      $("#edit-node-wrapper").show();
      $("#edit-keywords-wrapper").hide();
    } else if($("#edit-action").val() == 'taxonomy'){
      $("#edit-node-wrapper").hide();
      $("#edit-keywords-wrapper").show();
    } else {
      $("#edit-node-wrapper").hide();
      $("#edit-keywords-wrapper").hide();
    }
  });
});
