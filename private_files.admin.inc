<?php
/**
 * @file
 * Admin interface for private_files module
 */

/**
 * Renders the private files admin interface
 *
 * Renders two forms, the filter form and the file list
 * @todo Move actions and uploads to separate forms?
 *
 * @return array
 *   string containing the rendered forms
 */
function private_files_admin($form_state) {
  if (isset($form_state['values']['action'])
  && $form_state['values']['action'] == 'delete') {
    return private_files_confirm(
      $form_state, array_filter($form_state['values']['file_list']));
  }

  $path = drupal_get_path('module', 'private_files');
  drupal_add_js($path . '/private_files.js', 'module', 'header');
  module_load_include('inc', 'private_files', 'private_files.filters');
  $form = private_files_filter_form();
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['admin'] = private_files_admin_form();
  return $form;
}

/**
 * Annoying delete confirmation page
 */
function private_files_confirm($form_state, $files) {
  $form['file_list'] = array(
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );
  module_load_include('inc', 'private_files', 'private_files.db');
  foreach ($files as $fid) {
    $file = private_files_get_file($fid);
    $form['file_list'][$fid] = array(
      '#type' => 'hidden',
      '#value' => $fid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($file->filename) . "</li>\n",
    );
  }
  $form['operation'] = array(
    '#type' => 'hidden',
    '#value' => 'delete',
  );
  $form['#submit'][] = 'private_files_confirm_submit';
  return confirm_form(
    $form,
    t('Are you sure you want to delete these files?'),
    'admin/content/private_files',
    t('This action cannot be undone.'),
    t('Delete all'),
    t('Cancel')
  );
}

/**
 * Annoying delete confirmation submit handler
 */
function private_files_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    module_load_include('inc', 'private_files', 'private_files.db');
    foreach ($form_state['values']['file_list'] as $fid) {
      private_files_del_file($fid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/content/private_files';
}

/**
 * Finds files in a given directory and it's sub-directories,
 *
 * Starting from the specified root path, this function scans
 * for files recursively using sub-directory names as taxonomy terms
 * Any files found get checked and added to the database if necessary
 *
 * @param string $path
 *   the directory to read from
 * @param array $tax
 *   array of taxonomy terms for the current path
 */
function private_files_read_dir($path, $tax = array()) {
  $dir = dir($path);
  while (is_object($dir) && FALSE !== ($file = $dir->read())) {
    // Ignore hidden files.
    if (substr($file, 0, 1) != '.') {
      $filepath = $path . '/' . $file;
      if (is_dir($filepath)) {
        // Ignore system directories.
        $ignore = variable_get(
          'private_files_ignore',
          array(
            'css',
            'js',
            'imagecache',
            'imagefield_thumbs',
            'languages',
            'ctools',
            'tmp',
          ));

        if (!in_array($file, $ignore)) {
          // Add this directory as a taxonomy term then recurse down.
          $tax[] = $file;
          private_files_read_dir($filepath, $tax);
          array_pop($tax);
        }
      }
      else {
        // Add the files record here in case the file was
        // not uploaded the regular way.
        $fid = private_files_add_file($file, $path);
        // Add taxonomy terms if there are any.
        if (!empty($tax)) {
          private_files_add_taxonomy($fid, $tax);
        }
      }
    }
  }
}

/**
 * Gets file references for a given fid.
 *
 * @param integer $fid
 *   a file ID
 *
 * @return string
 */
function private_files_references($fid) {
  $ref = array();
  // Get an array of modules referencing this file.
  $refs = module_invoke_all('file_references', array('fid' => $fid));
  // But ignore our own references.
  unset($refs['private_files']);
  foreach ($refs as $module => $count) {
    if ($count) {
      $nodes = array();
      // Get the actual nodes referencing this file if the
      // module implements this hook, otherwise just show a count.
      if (module_hook($module, 'get_file_references')) {
        $nodes = module_invoke(
          $module, 'get_file_references', array('fid' => $fid));
        foreach ($nodes as $nid => $value) {
          // Load the node so we can get the title.
          $node = node_load($nid);
          $nodes[$nid] = l($nid, 'node/' . $nid . '/edit',
            array(
              'query' => drupal_get_destination(),
              'attributes' => array('title' => $node->title), ));
        }
        $ref[] = $module . ': ' . implode(', ', $nodes);
      }
      else {
        $ref[] = $module . ': ' . $count;
      }
    }
  }
  return implode('<br />', $ref);
}

/**
 * Defines the main private files admin interface
 *
 * This form provides the ability to upload files, along with a paginated
 * list of files upon which actions can be performed
 *
 * @return array
 *   forms API array
 */
function private_files_admin_form() {
  $form = array();

  // Include the db file here as private_files_read_dir is recursive.
  module_load_include('inc', 'private_files', 'private_files.db');

  // Get list of files in private dir to make sure they're all in the db.
  $private = variable_get('private_files_dir', '');
  if ($private) {
    private_files_read_dir($private);
  }

  // And the same for public files.
  $public = variable_get('file_directory_path', '');
  if ($public) {
    private_files_read_dir($public);
  }

  // Set enctype to allow file uploads.
  $form['#theme'] = 'private_files_list';

  $form['uploads'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Upload a new file'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  $form['uploads']['upload'] = array(
    '#type' => 'file',
    '#size' => 30,
  );

  // Show public/private checkbox if private_files_dir is set.
  if ($private) {
    $form['uploads']['private'] = array(
      '#type' => 'checkbox',
      '#title' => t('Private?'),
      '#default_value' => 0,
    );
  }

  $form['uploads']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Upload',
    '#submit' => array('private_files_admin_form_submit'),
  );

  // Now get the paginated file list from the db
  // This function does all the filtering.
  $files = private_files_get_files(private_files_header());

  if (count($files)) {

    $form['actions'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Actions'),
    );

    // Invoke file_operations hook to get a set of possible actions
    // that can be performed on files
    // We implement this hook locally too.
    $options = array();
    foreach (module_invoke_all('file_operations') as $action => $array) {
      $options[$action] = $array['label'];
    }

    $form['actions']['action'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => 'delete',
      '#prefix' => '<div class = "container-inline">',
    );

    $form['actions']['node'] = array(
      '#type' => 'textfield',
      '#title' => 'Node Title',
      '#size' => 40,
      '#autocomplete_path' => 'private_files/autocomplete-nid',
    );

    $form['actions']['keywords'] = array(
      '#type' => 'textfield',
      '#title' => 'Keywords (comma separated)',
      '#size' => 40,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Perform action'),
      '#suffix' => '</div>',
      '#submit' => array('private_files_admin_form_submit'),
    );

    foreach ($files as $fid => $path) {
      $file = basename($path);

      if (!is_file($path)) {
        // File disappeared, remove the reference.
        drupal_set_message(t(
          'File %file not found, deleting from db',
          array('%file' => $path)));
        private_files_del_file($fid);
      }
      else {
        // Set up the form array.
        $boxes[$fid] = '';
        // If it's an image, show an imagecache icon.
        if (exif_imagetype($path)) {
          $form['icon'][$fid] = array(
            '#value' =>
              theme('imagecache', 'private_files_thumb', $path, 'thumbnail'),
          );
        }
        if (dirname($path) == $private) {
          $form['private'][$fid] = array('#value' => 'Yes');
          $form['title'][$fid]
            = array('#value' => l($file, 'system/private_files/' . $file));
        }
        else {
          $form['private'][$fid] = array('#value' => 'No');
          $form['title'][$fid] = array('#value' => l($file, $path));
        }
        $form['size'][$fid] = array('#value' => filesize($path));
        $form['date'][$fid] = array('#value' => date('F d Y H:m',
          filemtime($path)));
        $roles = implode(', ', private_files_get_roles($fid));
        $form['roles'][$fid] = array('#value' => $roles);
        $form['kw'][$fid] = array(
          '#value' => implode(', ', private_files_get_taxonomy($fid)));
        $form['refs'][$fid] = array('#value' => private_files_references($fid));
      }
    }
  }

  // File selector checkboxes.
  $form['file_list'] = array('#type' => 'checkboxes', '#options' => $boxes);

  return $form;
}

/**
 * Handles admin form submissions
 *
 * Processes file uploads, as well as any actions performed on selected files
 */
function private_files_admin_form_submit($form, &$form_state) {
  // Handle uploads first.
  if (isset($_FILES['files']) && $_FILES['files']['name']['upload']) {
    private_files_upload($_FILES['files'], $form_state['values']['private']);
  }
  else {
    // Then do file actions.
    $operations = module_invoke_all('file_operations', $form_state);
    $operation = $operations[$form_state['values']['action']];
    $files = array_filter($form_state['values']['file_list']);
    if ($function = $operation['callback']) {
      // Add in callback arguments if present.
      if (isset($operation['callback arguments'])) {
        $args = array_merge(array($files), $operation['callback arguments']);
      }
      else {
        $args = array($files);
      }
      module_load_include('inc', 'private_files', 'private_files.operations');
      if (call_user_func_array($function, $args)) {
        drupal_set_message(t('The update has been performed'));
      }
      else {
        drupal_set_message(t('Unable to perform operation'), 'error');
      }
    }
    else {
      $form_state['rebuild'] = TRUE;
    }
  }
}

/**
 * Do the upload - mostly taken from core upload functions
 *
 * @param array $file
 *   data on the file we received
 * @param boolean $private
 *   if the file should be saved in the private area or not
 */
function private_files_upload($file, $private) {
  if (is_uploaded_file($file['tmp_name']['upload'])) {
    switch ($file['error']['upload']) {
      case UPLOAD_ERR_OK:
        break;

      case UPLOAD_ERR_INI_SIZE:
      case UPLOAD_ERR_FORM_SIZE:
        drupal_set_message(
          t('The file %file could not be saved, because it exceeds %maxsize, the maximum allowed size for uploads.',
            array(
              '%file' => $source,
              '%maxsize' => format_size(file_upload_max_size()),
            )
          ),
          'error');
        return 0;

      case UPLOAD_ERR_PARTIAL:
      case UPLOAD_ERR_NO_FILE:
        drupal_set_message(
          t('The file %file could not be saved because the upload did not complete.',
            array('%file' => $source)),
          'error');
        return 0;

      default:
        drupal_set_message(
          t('The file %file could not be saved. An unknown error has occurred.',
            array('%file' => $source)),
          'error');
        return 0;
    }

    $extensions = 'jpg jpeg gif png txt html doc xls pdf';

    $filename = file_munge_filename(
      trim(basename($file['name']['upload']), '.'), $extensions);

    // Rename potentially executable files, to help prevent exploits.
    if (preg_match('/\.(php|pl|py|cgi|asp|js)$/i', $filename)
    && (substr($filename, -4) != '.txt')) {
      $filename .= '.txt';
      // As the file may be named example.php.txt, we need to munge again to
      // convert to example.php_.txt, then create the correct destination.
      $filename = file_munge_filename($filename, $extensions);
    }

    if ($private) {
      $filepath = variable_get('private_files_dir', '');
    }
    else {
      $filepath = variable_get('file_directory_path', '');
    }
    if (!move_uploaded_file($file['tmp_name']['upload'], $filepath . '/' . $filename)) {
      form_set_error(
        'private_files',
        t('File upload error. Could not move uploaded file.'));
      watchdog(
        'file',
        'Upload error. Could not move file %file to %path dir.',
        array('%file' => $filename, '%path' => $filepath));
      return 0;
    }

    module_load_include('inc', 'private_files', 'private_files.db');
    private_files_add_file($filename, $filepath);
  }
  else {
    drupal_set_message('Non-uploaded file found');
  }
}

/**
 * Defines the form elements for the private files filefield widget
 *
 * @return array
 *   a forms API array
 */
function private_files_filefield_process($element, $edit, &$form_state, $form) {
  $element['filefield_private'] = array(
    '#weight' => 100.5,
    '#theme' => 'private_files_filefield_sources_element',
    '#filefield_source' => TRUE,
    '#filefield_sources_hint_text' => FILEFIELD_SOURCE_REFERENCE_HINT_TEXT,
  );
  $element['filefield_private']['autocomplete'] = array(
    '#type' => 'textfield',
    '#autocomplete_path' => 'filefield/private/'
      . $element['#type_name'] . '/' . $element['#field_name'],
    '#description' => 'Choose a file',
  );
  $element['filefield_private']['select'] = array(
    '#name' => implode('_', $element['#array_parents'])
      . '_autocomplete_select',
    '#type' => 'submit',
    '#value' => t('Select'),
    '#validate' => array(),
    '#submit' => array('node_form_submit_build_node'),
    '#name' => $element['#name'] . '[filefield_private][button]',
    '#ahah' => array(
      'path' => 'filefield/ahah/' . $element['#type_name']
        . '/' . $element['#field_name'] . '/' . $element['#delta'],
      'wrapper' => $element['#id'] . '-ahah-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  return $element;
}

/**
 * Processes filefield data received from the private files widget
 */
function private_files_filefield_value($element, &$item) {
  $ac = $item['filefield_private']['autocomplete'];
  if (isset($ac) && strlen($ac) > 0) {
    $matches = array();
    if (preg_match('/\[fid:(\d+)\]/', $ac, $matches)) {
      $fid = $matches[1];
      if ($file = field_file_load($fid)) {
        // Run all the normal validations, minus file size restrictions.
        if (isset($element['#upload_validators']['filefield_validate_size'])) {
          unset($element['#upload_validators']['filefield_validate_size']);
        }
        if (filefield_sources_element_validate($element, (object) $file)) {
          $item = array_merge($item, $file);
        }
      }
      else {
        form_error($element, t('The referenced file could not be used because the file does not exist in the database.'));
      }
    }
    // No matter what happens, clear the value from the autocomplete.
    $item['filefield_private']['autocomplete'] = '';
  }
}

/**
 * Generates the autocomplete list for the filefield widget
 */
function private_files_filefield_sources_private_autocomplete($type_name, $field_name, $filename) {
  $items = array();
  module_load_include('inc', 'private_files', 'private_files.db');
  $files = private_files_get_autocomplete($filename);
  foreach ($files as $fid => $file) {
    $items[$file->filename . " [fid:$fid]"]
      = theme('private_files_filefield_sources_autocomplete_item', $file);
  }
  drupal_json($items);
}

/**
 * Themes the output of a single item in the filefield autocomplete list
 *
 * @return string
 *   themed output of a single item in the autocomplete list
 */
function theme_private_files_filefield_sources_autocomplete_item($file) {
  $output = '<div class="filefield-source-private-item">';
  $output .= '<span class="filename">';
  $output .= $file->filename;
  $output .= '</span> <span class="filesize">(';
  $output .= format_size($file->filesize);
  $output .= ')</span>';
  $output .= '</div>';
  return $output;
}

/**
 * Themes the output of the filefield autocomplete field
 *
 * @return string
 *   themed output of the autocomplete field
 */
function theme_private_files_filefield_sources_element($element) {
  $element['autocomplete']['#field_suffix']
    = theme('submit', $element['select']);
  return '<div class="filefield-source filefield-source-private clear-block">'
    . theme('textfield', $element['autocomplete']) . '</div>';
}

/**
 * Theme the file listing for the admin interface with pagination
 *
 * @return string
 *   Rendered table of files with pager
 */
function theme_private_files_list($form) {
  $header = private_files_header();

  $output .= drupal_render($form['uploads']);
  $output .= drupal_render($form['actions']);
  if (isset($form['title']) && is_array($form['title'])) {
    foreach (element_children($form['title']) as $key) {
      $row = array();
      $row[] = drupal_render($form['file_list'][$key]);
      $row[] = drupal_render($form['icon'][$key]);
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['private'][$key]);
      $row[] = drupal_render($form['size'][$key]);
      $row[] = drupal_render($form['date'][$key]);
      $row[] = drupal_render($form['roles'][$key]);
      $row[] = drupal_render($form['kw'][$key]);
      $row[] = drupal_render($form['refs'][$key]);
      $rows[] = $row;
    }
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, 50, 0);
  }
  else {
    $output .= t('No files available');
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Provides table header data for file listing
 *
 * @return array
 *   table headers
 */
function private_files_header() {
  return array(
    theme('table_select_header_cell'),
    '',
    array('data' => t('Filename'), 'field' => 'f.filename', 'sort' => 'asc'),
    t('Private?'),
    t('Size'),
    t('Date modified'),
    t('Roles'),
    t('Keywords'),
    t('References'),
  );
}
