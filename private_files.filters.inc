<?php
/**
 * @file
 * filter functions
 */

/**
 * Lists file admin filters
 *
 * This is invoked by hook private_files_file_filters
 *
 * @return array
 *   filters
 */
function private_files_filters() {
  module_load_include('inc', 'private_files', 'private_files.db');
  $filters = array();
  $privdir = variable_get('private_files_dir', '');
  $pubdir = variable_get('file_directory_path', '');

  if ($privdir && $pubdir) {
    $filters['visibility'] = array(
      'title' => t('Visibility'),
      'where' => "f.filepath like '%s%%'",
      'options' => array($privdir => t('Private'), $pubdir => t('Public')),
    );
  }

  $filters['type'] = array(
    'title' => t('Type'),
    'where' => "f.filemime like '%s/%'",
    'options' => array(
      'application' => t('Documents'),
      'image' => t('Images'),
      'text' => t('Text files'),
    ),
  );

  $filters['extension'] = array(
    'title' => t('Extension'),
    'where' => "f.filepath like '%%%s'",
    'options' => private_files_extensions(),
  );

  $filters['taxonomy'] = array(
    'title' => t('Keywords'),
    'options' => private_files_terms(),
    'join' => "INNER JOIN {private_files_set} pfs ON f.fid = pfs.fid AND pfs.tid =  %d",
  );

  $filters['starts'] = array(
    'title' => t('First Char'),
    'options' => array_combine(
      array_merge(range('0', '9'), range('a', 'z')),
      array_merge(range('0', '9'), range('a', 'z'))
    ),
    'where' => "f.filename like '%s%%'",
  );

  $roles = user_roles(TRUE);
  if (count($roles)) {
    $filters['role'] = array(
      'title' => t('role'),
      'where' => '',
      'options' => $roles,
      'join' => 'INNER JOIN {private_files_role} pfr ON f.fid = pfr.fid AND pfr.rid = %d',
    );
  }

  return $filters;
}

/**
 * Defines the filter form which appears at the top of the admin interface
 *
 * @return array
 *   a forms API array
 */
function private_files_filter_form() {
  // Get filters applied in this session.
  $session = &$_SESSION['private_files_filter'];
  $session = is_array($session) ? $session : array();

  // Get our filters and anyone elses.
  $filters = module_invoke_all('file_filters');

  // Make it collapsed unless a filter has been applied.
  $collapsed = empty($session);

  // If filters have been applied, output what they are.
  $i = 0;
  $current = array();
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    $params = array(
      '%property' => $filters[$type]['title'],
      '%value' => $filters[$type]['options'][$value],
    );
    if ($i++ > 0) {
      $s = '<em>and</em> <strong>%property</strong> is <strong>%value</strong>';
    }
    else {
      $s = '<strong>%property</strong> is <strong>%value</strong>';
    }
    $current[] = array('#value' => t($s, $params));
    unset($filters[$type]);
  }

  // Now build the form.
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed,
    '#title' => t('Show only files where'),
    '#theme' => 'private_files_filters',
  );

  $form['filters']['current'] = $current;

  $form['#submit'][] = 'private_files_filter_form_submit';

  foreach ($filters as $key => $filter) {
    $names[$key] = $filter['title'];
    $form['filters']['status'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
    );
  }

  $form['filters']['filter'] = array(
    '#type' => 'radios',
    '#options' => $names,
  );

  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => (count($session) ? t('Refine') : t('Filter')),
  );

  if (count($session)) {
    $form['filters']['buttons']['undo'] = array(
      '#type' => 'submit',
      '#value' => t('Undo'),
    );
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }

  drupal_add_js('misc/form.js', 'core');
  return $form;
}

/**
 * Handles submissions from the filter form
 */
function private_files_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = module_invoke_all('file_filters');
  // Set the session variable appropriately.
  switch ($op) {
    case t('Filter'): case t('Refine'):
      if (isset($form_state['values']['filter'])) {
        $filter = $form_state['values']['filter'];
        $options = $filters[$filter]['options'];
        if (isset($options[$form_state['values'][$filter]])) {
          $_SESSION['private_files_filter'][]
            = array($filter, $form_state['values'][$filter]);
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['private_files_filter']);
      break;
    case t('Reset'):
      $_SESSION['private_files_filter'] = array();
      break;
    case t('Update');
      return;
  }
  $form_state['redirect'] = 'admin/content/private_files';
  return;
}

/**
 * Themes private files filter form
 *
 * @ingroup themeable
 *
 * @return string
 *   themed output of filter form
 */
function theme_private_files_filter_form($form) {
  $output = '<div id="private-files-filter">';
  $output .= drupal_render($form['filters']);
  $output .= '</div>';
  $output .= drupal_render($form);
  return $output;
}

/**
 * Themes private_files filters fieldset.
 *
 * @ingroup themeable
 *
 * @return string
 *   themed output of filter fieldset
 */
function theme_private_files_filters($form) {
  $output = '<ul class="clear-block">';
  if (!empty($form['current'])) {
    // Show currently defined filters.
    foreach (element_children($form['current']) as $key) {
      $output .= '<li>' . drupal_render($form['current'][$key]) . '</li>';
    }
  }

  $output .= '<li><dl class="multiselect">'
    . (!empty($form['current']) ? '<dt><em>' . t('and') . '</em> '
    . t('where') . '</dt>' : '') . '<dd class="a">';
  // Show filter options.
  foreach (element_children($form['filter']) as $key) {
    $output .= drupal_render($form['filter'][$key]);
  }
  $output .= '</dd>';

  $output .= '<dt>' . t('is') . '</dt><dd class="b">';
  // Show filter values.
  foreach (element_children($form['status']) as $key) {
    $output .= drupal_render($form['status'][$key]);
  }
  $output .= '</dd>';
  $output .= '</dl>';
  $output .= '<div class="container-inline" id="user-admin-buttons">';
  $output .= drupal_render($form['buttons']) . '</div>';
  $output .= '</li></ul>';

  return $output;
}
