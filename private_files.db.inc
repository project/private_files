<?php
/**
 * @file
 * Include file for private files database operations
 */

/**
 * Gets a file object for a given file and/or path
 *
 * @param mixed $file
 *   name or ID of the file
 * @param string $path
 *   path to the file - private_files_dir variable will be used if this is empty
 *
 * @return object
 *   a file object with an additional is_private attribute
 */
function private_files_get_file($file, $path = '') {
  $cache = 0;
  $cache_key = 'private_files_' . $file;
  if ($cache = cache_get($cache_key)) {
    $cache = $cache->data;
  }
  else {
    if (is_numeric($file)) {
      $result = db_query("SELECT * FROM {files} WHERE fid = %d", $file);
      $cache = db_fetch_object($result);
    }
    if (!$cache) {
      // Transliterate just in case.
      if (function_exists('transliteration_clean_filename')) {
        $file = transliteration_clean_filename($file);
      }
      // Use private_files_dir if no path was specified.
      $path = ($path ? $path : variable_get('private_files_dir', ''))
        . '/' . $file;
      $result = db_query("SELECT * FROM {files} WHERE filepath = '%s'", $path);
      $cache = db_fetch_object($result);
    }
    if ($cache) {
      if ($privdir = variable_get('private_files_dir', '') && $privdir) {
        $cache->is_private = strpos($cache->filepath, $privdir) === 0;
      }
      cache_set($cache_key, $cache);
    }
  }
  return $cache;
}

/**
 * Removes a file and its associated DB records and clears the relevant cache
 *
 * @todo use hook_file_delete?
 *
 * @param mixed $file
 *   name or ID of the file
 */
function private_files_del_file($file) {
  $file = private_files_get_file($file);
  // Let other modules do their thing.
  module_invoke_all('file_delete', $file);
  db_query("DELETE FROM {private_files_role} WHERE fid = %d", $file->fid);
  db_query("DELETE FROM {private_files_set} WHERE fid = %d", $file->fid);
  db_query("DELETE FROM {files} WHERE fid = %d", $file->fid);
  if (file_exists($file->filepath)) {
    unlink($file->filepath);
  }
  cache_clear_all('private_files_' . $file->fid, 'cache');
  cache_clear_all('private_files_' . $file->filename, 'cache');
}

/**
 * Adds a file to the files table
 *
 * @param string $filename
 *   name of the file
 * @param string $path
 *   path to the file - private_files_dir variable will be used if this is empty
 *
 * @return integer
 *   a File ID
 */
function private_files_add_file($filename, $path = '') {
  global $user;
  $filepath = ($path ? $path : variable_get('private_files_dir', ''))
    . '/' . $filename;
  // If the file exists on disk but not in the files table
  // then add it to the files table.
  if (is_file($filepath) && !$ob = private_files_get_file($filename, $path)) {
    $ob = new stdClass();
    $ob->uid = $user->uid;
    $ob->filename = $filename;
    $ob->filepath = $filepath;
    $ob->filemime = file_get_mimetype($filename);
    $ob->filesize = filesize($filepath);
    $ob->status = FILE_STATUS_PERMANENT;
    $ob->timestamp = filemtime($filepath);
    drupal_set_message(t(
      'Adding %file to database',
      array('%file' => $filename)));
    drupal_write_record('files', $ob);
  }
  return $ob->fid;
}

/**
 * Builds a query for file listing
 *
 * Building on a simple query to get files from the files table, this
 * function adds any filters that have been selected from the admin
 * interface to restrict the number of files returned. The result is passed
 * through pager_query for pagination.
 *
 * @param array $header
 *   The table headings array
 *
 * @return array
 *   filenames keyed by file ID
 */
function private_files_get_files($header) {
  $filters = private_files_filters();
  $private = variable_get('private_files_dir', '');
  $public = variable_get('file_directory_path', '');

  $where = $args = $join = array();

  // For each filter being used by the session,
  // extract the query criteria.
  $session = $_SESSION['private_files_filter'];
  $session = $session ? $session : array();
  foreach ($session as $filter) {
    list($key, $value) = $filter;
    if ($filters[$key]['where']) {
      $where[] = $filters[$key]['where'];
      $args[] = $value;
    }
    if ($filters[$key]['join']) {
      // Put joins at the start so args are in the right order.
      array_unshift($join, $filters[$key]['join']);
      array_unshift($args, $value);
    }
  }

  // Implode the arrays into query fragments.
  $join = !empty($join) ? ' ' . implode(' ', array_unique($join)) : '';
  $where = !empty($where) ? 'AND ' . implode(' AND ', $where) : '';

  // This SQL statement can have no whitespace at the start
  // otherwise it breaks pager_query
  $sql = "SELECT f.fid, f.filepath FROM {files} f " . $join . "
    WHERE  f.status = 1 " . $where;

  $sql .= tablesort_sql($header);

  // @TODO allow changing files per page.
  $result = pager_query($sql, 50, 0, $query_count, $args);

  $files = array();
  while ($file = db_fetch_object($result)) {
    $files[$file->fid] = $file->filepath;
  }
  return $files;
}

/**
 * Builds a files array for filefield autocomplete
 *
 * @param string $filename
 *   pattern to match filenames
 *
 * @return array
 *   files matching the pattern
 */
function private_files_get_autocomplete($filename) {
  $result = db_query_range("
    SELECT f.*
    FROM   {files} f
    WHERE  f.filepath LIKE '%s%%'
    AND    f.filename LIKE '%s%%'
    AND    f.status = 1
    ORDER BY f.filename",
    variable_get('private_files_dir', ''), $filename, 0, 30);
  while ($file = db_fetch_object($result)) {
    $files[$file->fid] = $file;
  }
  return $files;
}

/**
 * Builds an array of nodes for admin interface autocomplete
 *
 * @param string $string
 *   pattern to match node titles
 *
 * @return array
 *   nodes
 */
function private_files_get_node_autocomplete($string) {
  $select = "
    SELECT nid, title
    FROM {node}
    WHERE LOWER(title) LIKE LOWER('%%%s%%')";

  $result = db_query_range($select, $string, 0, 30);
  $matches = array();
  while ($row = db_fetch_object($result)) {
    $matches[$row->nid] = $row->title;
  }
  return $matches;
}

/**
 * Adds a role to a file
 *
 * @param integer $fid
 *   File ID
 * @param integer $rid
 *   Role ID
 */
function private_files_add_role($fid, $rid) {
  db_query("
    INSERT INTO {private_files_role} VALUES (%d, %d)",
    array($fid, $rid)
  );
  // Clear all cache so access is recalculated.
  cache_clear_all('private_files_', 'cache', TRUE);
}

/**
 * Deletes a role from a file
 *
 * @param integer $fid
 *   File ID
 * @param integer $rid
 *   Role ID
 */
function private_files_del_role($fid, $rid) {
  db_query("
    DELETE FROM {private_files_role} WHERE fid = %d AND rid = %d",
    array($fid, $rid)
  );
  // Clear all cache so access is recalculated.
  cache_clear_all('private_files_', 'cache', TRUE);
}

/**
 * Gets the roles attached to a given file ID
 *
 * @param integer $fid
 *   File ID
 *
 * @return array
 *   roles attached to this file
 */
function private_files_get_roles($fid) {
  $cache_key = 'private_files_role_' . $fid;
  if ($cache = cache_get($cache_key)) {
    $roles = $cache->data;
  }
  else {
    $res = db_query("
      SELECT p.rid, r.name
      FROM   {private_files_role} p, {role} r
      WHERE  p.fid = %d
      AND    p.rid = r.rid",
      $fid
    );
    $roles = array();
    while ($role = db_fetch_object($res)) {
      $roles[$role->rid] = $role->name;
    }
    cache_set($cache_key, $roles);
  }
  return $roles;
}

/**
 * Gets a list of file extensions for use in query filter
 *
 * @return array
 *   file extensions
 */
function private_files_extensions() {
  $return = array();
  $result = db_query("SELECT filepath FROM {files}");
  while ($path = db_result($result)) {
    if ($ext = pathinfo($path, PATHINFO_EXTENSION)) {
      $return[strtolower($ext)] = strtoupper($ext);
    }
  }
  return $return;
}

/**
 * Defines the taxonomy vocabulary
 *
 * Returns the vocabulary ID, creating the vocabulary if it doesn't exist
 *
 * @return integer
 *   the vocabulary ID
 */
function private_files_vocab() {
  $vocab = new stdClass();
  if (!$vocab->vid = cache_get('taxofiles_vocab')->data) {
    $result = db_query("SELECT vid FROM {vocabulary} WHERE name = 'Taxofiles'");
    $vocab->vid = db_result($result);
    if (!$vocab->vid) {
      $vocab->name = 'Taxofiles';
      $vocab->relations = 1;
      $vocab->hierarchy = 0;
      $vocab->multiple = 1;
      $vocab->module = 'private_files';
      drupal_write_record('vocabulary', $vocab);
    }
    cache_set('taxofiles_vocab', $vocab->vid, 'cache');
  }
  return $vocab->vid;
}

/**
 * Returns all taxonomy terms for our vocab
 *
 * @return array
 *   terms
 */
function private_files_terms() {
  $result = db_query("
    SELECT tid, name
    FROM   {term_data}
    WHERE  vid = %d", private_files_vocab()
  );
  $terms = array();
  while ($row = db_fetch_object($result)) {
    $terms[$row->tid] = $row->name;
  }
  return $terms;
}

/**
 * Adds taxonomy terms to a file
 *
 * @param integer $fid
 *   File ID
 * @param mixed $keywords
 *   array or comma separated string of keywords
 */
function private_files_add_taxonomy($fid, $keywords) {
  $vocab = private_files_vocab();
  $terms = private_files_terms();
  $fileterms = private_files_get_taxonomy($fid);
  $data = new stdClass();
  if (!is_array($keywords)) {
    $keywords = explode(',', $keywords);
  }
  foreach ($keywords as $term) {
    $term = trim($term);
    if (!$data->tid = array_search($term, $terms)) {
      $data->vid = $vocab;
      $data->name = $term;
      drupal_write_record('term_data', $data);
      $data->parent = 0;
      drupal_write_record('term_hierarchy', $data);
    }
    if (array_search($term, $fileterms) === FALSE) {
      $data->fid = $fid;
      drupal_write_record('private_files_set', $data);
    }
  }
}

/**
 * Gets terms attached to a file
 *
 * @param integer $fid
 *   File ID
 *
 * @return array
 *   terms attached to this file
 */
function private_files_get_taxonomy($fid) {
  $result = db_query("
    SELECT td.name
    FROM   {term_data} td, {private_files_set} pfs
    WHERE  pfs.fid = %d
    AND    pfs.tid = td.tid", $fid);
  $return = array();
  while ($term = db_result($result)) {
    $return[] = $term;
  }
  return $return;
}
